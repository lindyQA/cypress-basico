describe("Tickets", () => {
  beforeEach(() =>
    cy.visit(
      "https://ticketbox-backstopjs-tat.s3.eu-central-1.amazonaws.com/index.html"
    )
  );

  it("fills all the text input filds", () => {
    const firstName = "Lindinha";
    const lastName = "Souza";
    const email = "test@ticketbox.com";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type(email);
    cy.get("#requests").type("Carnivora");
    cy.get("#signature").type(firstName.concat(" ").concat(lastName));
  });

  it("Select two tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("select 'vip' ticket type", () => {
    cy.get("#vip").check();
  });

  it("select 'social media' checkbox", () => {
    cy.get("#social-media").check();
  });

  it("selects 'friends, and 'publication' then uncheck 'friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });

  it("Has 'TICKETBOX', header's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("alerts on invalid email", () => {
    cy.get("#email").as("email").type("teste.gmail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email").clear().type("test@talking.com.br");

    cy.get("#email.invalid").should("not.exist");
  });

  it("fills and reset the form", () => {
    const firstName = "Lindinha";
    const lastName = "Souza";
    const email = "test@ticketbox.com";
    const fullname = firstName.concat(" ").concat(lastName);

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type(email);
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("Wine");
    cy.get("#signature").type(firstName.concat(" ").concat(lastName));

    cy.get(".agreement p").should(
      "contain",
      `I, ${fullname}, wish to buy 2 VIP tickets.`
    );
    cy.get("#agree").click();
    cy.get("#signature").type(fullname);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[Type='reset']").click();
    cy.get("@submitButton").should("be.disabled");
  });

  it("fills mandatory fields using support command", () => {
    const customer = {
      firstName: "Jonh",
      lastName: "Silva",
      email: "teste@teste.com",
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");
  });
});
